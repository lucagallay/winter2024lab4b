import java.util.Scanner;

public class VirtualPetApp{
	public static void main(String[] args){
		//animal array
		Seals[] pod = new Seals[4];

		
		Scanner inputer = new Scanner(System.in);
		
		for(int i = 0; i < pod.length; i++){
			
			//asks user to fill out fields
			System.out.println("What is the size of your seal?");
				String size = inputer.next();
				
			System.out.println("What is the whisker length (in cm) of your seal?");
				int whiskerLength = inputer.nextInt();
				
			System.out.println("What is the fin length (in cm) of your seal?");
				int fin = inputer.nextInt();
				
			System.out.println("Is your seal tropical? (Input 'true' or 'false')");
				boolean isTropical = inputer.nextBoolean();
			
			//seperates print spaces to make it clear to user they are inputing a new seal
			System.out.println("");
			
			//temp array to store data
			//overwritten each iteration
			Seals temp = new Seals(size, whiskerLength, fin, isTropical);
			
			//stores into array using i
			pod[i] = temp;
		}

		System.out.println("Field of last seal: " + 
		pod[pod.length-1].getSize() + " " + 
		pod[pod.length-1].getWhiskerLength() + " " +
		pod[pod.length-1].getFin() + " " +
		pod[pod.length-1].getIsTropical() + "\r\n");

		System.out.println("What is the fin length (in cm) of the last seal? (again)");
		pod[pod.length-1].setFin(inputer.nextInt());

		System.out.println("Field of last seal (updated): " + 
		pod[pod.length-1].getSize() + " " + 
		pod[pod.length-1].getWhiskerLength() + " " +
		pod[pod.length-1].getFin() + " " +
		pod[pod.length-1].getIsTropical() + "\r\n");

		
		
	}

}