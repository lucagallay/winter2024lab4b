public class Seals{
	//size does nothing, originally meant to be evaluated in catchFish function, but the idea was scrapped due to time constraints
	//currently, it only acts as flavour text for typeOfSeal function
	private String size;
	private int whiskerLength;
	private int fin;
	private boolean isTropical;
	
	//constructor
	public Seals(String size, int whiskerLength, int fin, boolean isTropical){
		this.size = size;
		this.whiskerLength = whiskerLength;
		this.fin = fin;
		this.isTropical = isTropical;
	}
	
	//just returns the fields to be print
	public String typeOfSeal(){
		return "SEAL DESCRIPTION" + 
		"\r\n Is your seal tropical: " + isTropical +
		"\r\n Size: " + this.size +
		"\r\n Length of whiskers: " + this.whiskerLength + " cm" +
		"\r\n Length of fin: " + this.fin + " cm";
	}

	public String getSize() {
		return size;
	}

	

	public int getWhiskerLength() {
		return whiskerLength;
	}

	public int getFin() {
		return fin;
	}

	public void setFin(int fin) {
		this.fin = fin;
	}

	public boolean getIsTropical() {
		return isTropical;
	}










	
	public String catchFish(){
		//storage string
		String sealCatches = "Your seal caught: ";
		
		//different fishes depends on wheter if the seal is tropical or not
		if(isTropical){
			//fish species depends on the stats of seal
			//should be able to catch up to 2 species of fish
			//based on the size of their whisker and fin
			
			//cluster 1: checks whisker size
			if(whiskerLength <= 5)
				sealCatches += "\r\n Barramundi";
			else if(whiskerLength > 5)
				sealCatches += "\r\n Mahi Mahi";
			else
				sealCatches += "\r\n nothing :(";
			
			//cluster 2: checks fin size
			if(fin <= 10)
				sealCatches += "\r\n Tuna";
			else if(fin > 30)
				sealCatches += "\r\n Swordfish";
			else
				sealCatches = "\r\n nothing :(";
		}
		else{
			//cluster 1: checks whisker size
			if(whiskerLength <= 10)
				sealCatches += "\r\n Cod";
			else if(whiskerLength > 10)
				sealCatches += "\r\n Artic Char";
			else
				sealCatches += "\r\n nothing :(";
			
			//cluster 2: checks fin size
			if(fin <= 10)
				sealCatches += "\r\n Flounder";
			else if(fin > 10)
				sealCatches += "\r\n Salmon";
			else
				sealCatches = "\r\n nothing :(";
		}
		
		//returns storage string to be printed
		return sealCatches;
	}
}